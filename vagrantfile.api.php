<?php
/**
 * @file
 * Hooks provided by this module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Acts on Vagrantfiles being loaded from the database.
 *
 * This hook is invoked during Vagrantfile loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array $vagrantfiles
 *   An array of Vagrantfile entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_vagrantfile_load(array $vagrantfiles) {
  $result = db_query('SELECT vfid, foo FROM {mytable} WHERE vfid IN(:ids)', array(':ids' => array_keys($vagrantfiles)));
  foreach ($result as $record) {
    $vagrantfiles[$record->vfid]->foo = $record->foo;
  }
}

/**
 * Responds when a Vagrantfile is inserted.
 *
 * This hook is invoked after the Vagrantfile is inserted into the database.
 *
 * @param Vagrantfile $vagrantfile
 *   The Vagrantfile that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_vagrantfile_insert(Vagrantfile $vagrantfile) {
  db_insert('mytable')
    ->fields(array(
      'id' => entity_id('vagrantfile', $vagrantfile),
      'extra' => print_r($vagrantfile, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a Vagrantfile being inserted or updated.
 *
 * This hook is invoked before the Vagrantfile is saved to the database.
 *
 * @param Vagrantfile $vagrantfile    'unique keys' => array(
      'name' => array('name'),
    ),

 *   The Vagrantfile that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_vagrantfile_presave(Vagrantfile $vagrantfile) {
  $vagrantfile->name = 'foo';
}

/**
 * Responds to a Vagrantfile being updated.
 *
 * This hook is invoked after the Vagrantfile has been updated in the database.
 *
 * @param Vagrantfile $vagrantfile
 *   The Vagrantfile that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_vagrantfile_update(Vagrantfile $vagrantfile) {
  db_update('mytable')
    ->fields(array('extra' => print_r($vagrantfile, TRUE)))
    ->condition('id', entity_id('vagrantfile', $vagrantfile))
    ->execute();
}

/**
 * Responds to Vagrantfile deletion.
 *
 * This hook is invoked after the Vagrantfile has been removed from the database.
 *
 * @param Vagrantfile $vagrantfile
 *   The Vagrantfile that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_vagrantfile_delete(Vagrantfile $vagrantfile) {
  db_delete('mytable')
    ->condition('vfid', entity_id('vagrantfile', $vagrantfile))
    ->execute();
}

/**
 * Alter Vagrantfile forms.
 *
 * Modules may alter the Vagrantfile entity form by making use of this hook or
 * the entity bundle specific hook_form_vagrantfile_edit_BUNDLE_form_alter().
 * #entity_builders may be used in order to copy the values of added form
 * elements to the entity, just as documented for
 * entity_form_submit_build_entity().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function hook_form_vagrantfile_form_alter(&$form, &$form_state) {
  // Your alterations.
}

/**
 * Act on a Vagrantfile that is being assembled before rendering.
 *
 * @param $Vagrantfile
 *   The Vagrantfile entity.
 * @param $view_mode
 *   The view mode the Vagrantfile is rendered in.
 * @param $langcode
 *   The language code used for rendering.
 *
 * The module may add elements to $Vagrantfile->content prior to rendering. The
 * structure of $Vagrantfile->content is a renderable array as expected by
 * drupal_render().
 *
 * @see hook_entity_prepare_view()
 * @see hook_entity_view()
 */
function hook_vagrantfile_view($Vagrantfile, $view_mode, $langcode) {
  $Vagrantfile->content['my_additional_field'] = array(
    '#markup' => $additional_field,
    '#weight' => 10,
    '#theme' => 'mymodule_my_additional_field',
  );
}

/**
 * Alter the results of entity_view() for Vagrantfiles.
 *
 * @param $build
 *   A renderable array representing the Vagrantfile content.
 *
 * This hook is called after the content has been assembled in a structured
 * array and may be used for doing processing which requires that the complete
 * Vagrantfile content structure has been built.
 *
 * If the module wishes to act on the rendered HTML of the Vagrantfile rather than
 * the structured content array, it may use this hook to add a #post_render
 * callback. Alternatively, it could also implement hook_preprocess_Vagrantfile().
 * See drupal_render() and theme() documentation respectively for details.
 *
 * @see hook_entity_view_alter()
 */
function hook_vagrantfile_view_alter($build) {
  if ($build['#view_mode'] == 'full' && isset($build['an_additional_field'])) {
    // Change its weight.
    $build['an_additional_field']['#weight'] = -10;
    // Add a #post_render callback to act on the rendered HTML of the entity.
    $build['#post_render'][] = 'my_module_post_render';
  }
}

/**
 * Define default Vagrantfile type configurations.
 *
 * @return
 *   An array of default vagrantfile types, keyed by machine names.
 *
 * @see hook_default_vagrantfile_type_alter()
 */
function hook_default_vagrantfile_type() {
  $defaults['main'] = entity_create('vagrantfile_type', array(
    // ...
  ));
  return $defaults;
}

/**
 * Alter default Vagrantfile type configurations.
 *
 * @param array $defaults
 *   An array of default Vagrantfile types, keyed by machine names.
 *
 * @see hook_default_vagrantfile_type()
 */
function hook_default_vagrantfile_type_alter(array &$defaults) {
  $defaults['main']->name = 'custom name';
}

/**
 * Act after rebuilding default Vagrantfile types.
 *
 * This hook is invoked by the entity module after default Vagrantfile type
 * have been rebuilt; i.e. defaults have been saved to the database.
 *
 * @param $vagrantf_files
 *   The array of default Vagrantfile type which have been inserted or
 *   updated, keyed by name.
 * @param $originals
 *   An array of original Vagrantfile type keyed by name; i.e. the Vagrantfile
 *   type before the current defaults have been applied. For inserted
 *   Vagrantfile types, no original is available.
 *
 * @see hook_default_vagrantfile_type()
 * @see entity_defaults_rebuild()
 */
function hook_vagrantfile_type_defaults_rebuild($vagrantf_files, $originals) {
}

/**
 * @}
 */
