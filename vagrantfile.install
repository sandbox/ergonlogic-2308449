<?php

/**
 * @file
 * Install, update and uninstall functions for the vagrantfile module.
 */

/**
 * Implements hook_uninstall().
 */
function vagrantfile_uninstall() {
  // Bypass entity_load() as we cannot use it here.
  $types = db_select('vagrantfile_type', 'et')
    ->fields('et')
    ->execute()
    ->fetchAllAssoc('name');

  foreach ($types as $name => $type) {
    field_attach_delete_bundle('vagrantfile', $name);
  }
}

/**
 * Implements hook_schema().
 */
function vagrantfile_schema() {
  $schema['vagrantfile'] = array(
    'description' => 'Stores vagrantfile items.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique vagrantfile item ID.',
      ),
      'name' => array(
        'description' => 'The machine-readable name of this vagrantfile.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The human-readable name of this vagrantfile.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'type' => array(
        'description' => 'The Vagrantfile type of this vagrantfile.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'foreign keys' => array(
      'type' => array(
        'vagrantfile_type' => 'type'
      ),
    ),
    'unique keys' => array(
      'name' => array('name'),
    ),
    'primary key' => array('id'),
  );

  $schema['vagrantfile_type'] = array(
    'description' => 'Stores information about all defined vagrantfile types.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique vagrantfile type ID.',
      ),
      'type' => array(
        'description' => 'The machine-readable name of this vagrantfile type.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The human-readable name of this vagrantfile type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'provider_name' => array(
        'description' => 'The machine-name used by the Vagrant provider.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'status' => array(
        'description' => 'The exportable status of the entity.',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        // Set the default to ENTITY_CUSTOM without using the constant as it is
        // not safe to use it at this point.
        'default' => 0x01,
      ),
      'module' => array(
        'description' => 'The name of the providing module if the entity has been defined in code.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('id'),
    'unique keys' => array(
      'type' => array('type'),
    ),
  );

  return $schema;
}

