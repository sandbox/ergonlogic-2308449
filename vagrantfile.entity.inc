<?php

/**
 * @file
 * Entity API hooks and classes for the vagrantfile module.
 */

/**
 * Implements hook_entity_info().
 */
function vagrantfile_entity_info() {
  $return['vagrantfile'] = array(
    'label' => t('Vagrantfile'),
    'plural label' => t('Vagrantfiles'),
    'description' => t('An entity type used by Hosting Vagrant to configure VMs.'),
    'entity class' => 'Vagrantfile',
    'controller class' => 'EntityAPIControllerExportable',
    'base table' => 'vagrantfile',
    'fieldable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'name' => 'name',
      'bundle' => 'type',
    ),
    // Make use the class' label() and uri() implementation by default.
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
    'bundles' => array(),
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    'access callback' => 'vagrantfile_access',
/*    'admin ui' => array(
      'path' => 'admin/hosting/vagrantfiles',
      'file' => 'vagrantfile.admin.inc',
      'controller class' => 'VagrantfileUIController',
      'menu wildcard' => '%vagrantfile',
    ),
*/    'module' => 'vagrantfile',
  );
  $return['vagrantfile_type'] = array(
    'label' => t('Vagrantfile type'),
    'plural label' => t('Vagrantfile types'),
    'entity class' => 'VagrantfileType',
    'controller class' => 'EntityAPIControllerExportable',
    'base table' => 'vagrantfile_type',
    'fieldable' => FALSE,
    'exportable' => TRUE,
    'bundle of' => 'vagrantfile',
    'entity keys' => array(
      'id' => 'id',
      'name' => 'type',
      'label' => 'label',
    ),
    'access callback' => 'vagrantfile_type_access',
    'admin ui' => array(
      'path' => 'admin/hosting/vagrantfile_types',
      'file' => 'vagrantfile_type.admin.inc',
      'controller class' => 'VagrantfileTypeUIController',
      'menu wildcard' => '%vagrantfile_type',
    ),
    'module' => 'vagrantfile',
  );

  // Add bundle info but bypass entity_load() as we cannot use it here.
  $types = db_select('vagrantfile_type', 'vt')
    ->fields('vt')
    ->execute()
    ->fetchAllAssoc('type');

  foreach ($types as $type => $info) {
    $return['vagrantfile']['bundles'][$info->type] = array(
      'label' => $info->label,
      'admin' => array(
        'path' => 'admin/hosting/vagrantfile_types/manage/%vagrantfile_type',
        'real path' => 'admin/hosting/vagrantfile_types/manage/' . $info->type,
        'bundle argument' => 4,
        'access arguments' => array('administer vagrantfile types'),
      ),
    );
  }
/*
  // Support entity cache module.
  if (module_exists('entitycache')) {
    $return['vagrantfile']['field cache'] = FALSE;
    $return['vagrantfile']['entity cache'] = TRUE;
  }
*/
  return $return;
}

/**
 * Main class for Vagrantfile entities.
 */
class Vagrantfile extends Entity {

  public function __construct(array $values = array(), $entityType = NULL) {
    parent::__construct($values, 'vagrantfile');
  }

  /**
   * Specifies the default label, which is picked up by label() by default.
   */
  protected function defaultLabel() {
    $vagrantfile_type = vagrantfile_get_types($this->type);
    return $vagrantfile_type->label;
  }

  /**
   * Specifies the default uri, which is picked up by uri() by default.
   */
#  protected function defaultURI() {
#    return array('path' => 'custom/' . $this->identifier());
#  }
}

/**
 * Main class for Vagrantfile type entities.
 */
class VagrantfileType extends Entity {

  public function __construct(array $values = array(), $entityType = NULL) {
    parent::__construct($values, 'vagrantfile_type');
  }

  /**
   * Returns whether the profile type is locked, thus may not be deleted or renamed.
   *
   * Vagrantfile types provided in code are automatically treated as locked, as well
   * as any fixed profile type.
   */
  public function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }

}

/**
 * Gets an array of all vagrantfile types, keyed by name.
 *
 * @param $name
 *   If set, the vagrantfile type with the given name is returned.
 */
function vagrantfile_get_types($type = NULL) {
  $vagrantfile_types = entity_load_multiple_by_name('vagrantfile_type', isset($type) ? array($type) : FALSE);
  return isset($type) ? reset($vagrantfile_types) : $vagrantfile_types;
}

/**
 * Menu argument loader; Load a vagrantfile type by string.
 *
 * @param $type
 *   The machine-readable name of a vagrantfile type to load.
 * @return
 *   A vagrantfile type array or FALSE if $type does not exist.
 */
function vagrantfile_type_load($type) {
  return vagrantfile_get_types($type);
}

/**
 * Load multiple vagrantfiles based on certain conditions.
 *
 * @param $ids
 *   An array of entity IDs.
 * @param $conditions
 *   An array of conditions to match against the {entity} table.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset.
 * @return
 *   An array of vagrantfile objects, indexed by id.
 */
function vagrantfile_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('vagrantfile', $ids, $conditions, $reset);
}

/**
 * Load vagrantfiles by name.
 *
 * @param $name
 *   (optional) The name for this vagrantfile. If no name is given all existing
 *   vagrantfiles are returned.
 *
 * @return Vagrantfile
 *   Returns a fully-loaded Vagrantfile if a name is passed. Else an array
 *   containing all Vagrantfiles is returned.
 *
 */
function vagrantfile_load($name = NULL) {
  // Replace dashes with underscores so this can be used as menu argument
  // loader too.
  $vagrantfiles = entity_load_multiple_by_name('vagrantfile', isset($name) ? array(strtr($name, array('-' => '_'))) : FALSE);
  if (isset($name)) {
    return isset($vagrantfiles[$name]) ? $vagrantfiles[$name] : FALSE;
  }
  return $vagrantfiles;
}

/**
 * Delete multiple vagrantfiles.
 *
 * @param $ids
 *   An array of vagrantfile IDs.
 */
function vagrantfile_delete_multiple(array $ids) {
  entity_get_controller('vagrantfile')->delete($ids);
}

/**
 * Saves a Vagrantfile type to the database.
 */
function vagrantfile_type_save($type, $label, $provider_name = NULL) {
  $values = array(
    'type' => $type,
    'label' => $label,
    'provider_name' => $provider_name,
  );
  $vagrantfile_type = new VagrantfileType($values);
  $vagrantfile_type->save();
  return $vagrantfile_type;
}

/**
 * Instantiates a new Vagrantfile entity.
 */
function vagrantfile_create($name, $label, $type) {
  $values = array(
    'name' => $name,
    'label' => $label,
    'type' => $type,
  );

  return new Vagrantfile($values);
}

/**
 * Saves a Vagrantfile to the database.
 */
function vagrantfile_save(Vagrantfile &$vagrantfile) {
  $vagrantfile->save();
}

/**
 * Access callback for the entity API.
 */
function vagrantfile_type_access($op, $type = NULL, $account = NULL) {
  return user_access('administer vagrantfile types', $account);
}

/**
 * Access callback for the entity API.
 */
function vagrantfile_access($op, $type = NULL, $account = NULL) {
  return user_access('administer vagrantfiles', $account);
}

