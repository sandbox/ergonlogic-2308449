<?php

/**
 * @file
 * Vagrantfile editing UI.
 */

/**
 * UI controller.
 */
class VagrantfileUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage Vagrantfiles.';
    dsm($items);
    return $items;
  }
}

/**
 * Generates the Vagrantfile type editing form.
 */
function vagrantfile_form($form, &$form_state, $vagrantfile_type, $op = 'edit') {
  if ($op == 'clone') {
    $vagrantfile_type->label .= ' (cloned)';
    $vagrantfile_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
#    '#default_value' => isset($vagrantfile_type->label) ? $vagrantfile_type->label : '',
    '#description' => t('The human-readable name of this Vagrantfile.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
/*  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($vagrantfile_type->type) ? $vagrantfile_type->type : '',
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'vagrantfile_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this Vagrantfile type. It must only contain lowercase letters, numbers, and underscores.'),
  );
  // Machine-readable type name.
  $form['provider_name'] = array(
    '#title' => t('Vagrant provider machine-name'),
    '#type' => 'textfield',
    '#default_value' => isset($vagrantfile_type->provider_name) ? $vagrantfile_type->provider_name : NULL,
    '#description' => t('The name machine-name used by the Vagrant provider.'),
    '#required' => TRUE,
    '#size' => 30,
  );
*/
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Vagrantfile'),
    '#weight' => 40,
  );

  return $form;
}

/**
 * Form API submit callback for the provider form.
 */
function vagrantfile_form_submit(&$form, &$form_state) {
  $vagrantfile_type = entity_ui_form_submit_build_entity($form, $form_state);
  $vagrantfile_type->save();
  $form_state['redirect'] = 'admin/hosting/vagrantfile_types';
}

/**
 * Form API submit callback for the delete button.
 */
function vagrantfile_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/hosting/vagrantfile_types/manage/' . $form_state['vagrantfile_type']->type . '/delete';
}
